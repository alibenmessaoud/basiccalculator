package bean;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.ToggleEvent;

/**
 *
 * @author mirac
 */
@ManagedBean(name = "calculatorBean")
@RequestScoped
public class CalculatorBean {
    
    private int number1, number2, result;
    private String pointer;
    
    public CalculatorBean() {
        result = 0;
    }

    public void handleToggle(ToggleEvent event) {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO,
                "Calculator Fieldset Toggled", "Visibility:" + event.getVisibility());

        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
    
    public int calculate() {
        switch(pointer) {
            case "*":
                result = (number1 * number2);
                return result;
                
            case "/":
                result = (number1 / number2);
                return result;
                
            case "+":
                result = (number1 + number2);
                return result;
                
            case "-":
                result = (number1 - number2);
                return result;
        }
        return 0;
    }
    
    public int getNumber1() {
        return number1;
    }

    public void setNumber1(int number1) {
        this.number1 = number1;
    }

    public int getNumber2() {
        return number2;
    }

    public void setNumber2(int number2) {
        this.number2 = number2;
    }

    public String getPointer() {
        return pointer;
    }

    public void setPointer(String pointer) {
        this.pointer = pointer;
    }
    
    public int getResult() {
        return result;
    }

    public void setResult(int result) {
        this.result = result;
    }
}
